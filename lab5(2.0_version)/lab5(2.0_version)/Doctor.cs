﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Doctor
{

    public string first_name, last_name, specialty, time_of_receipt_from, time_of_receipt_to;
    public Doctor(string first_name, string last_name, string specialty)
    {
        this.first_name = first_name;
        this.last_name = last_name;
        this.specialty = specialty;
        time_of_receipt_from = "8";
        time_of_receipt_to = "-17";
    }
    public override string ToString()
    {
        return String.Format("{0,10} {1,15} {2,20} {3,15} {4,0}", first_name, last_name, specialty, time_of_receipt_from, time_of_receipt_to);
    }
}

