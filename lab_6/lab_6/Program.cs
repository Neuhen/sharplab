﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_6
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList<Book> myList = new LinkedList<Book>();
             Book b1 = new Book() { BookName = "Властелин Колец", Price = 200 };
             Book b2 = new Book() { BookName = "Три мушкетера", Price = 300 };
             Book b3 = new Book() { BookName = "Гарри Поттер", Price = 400 };
             Book b4 = new Book() { BookName = "Волшебник", Price = 800 };
            myList.AddFirst(b1);
            myList.AddLast(b2);

            LinkedListNode<Book> node2 = myList.Find(b2);
            myList.AddBefore(node2, b3);

            LinkedListNode<Book> node1 = myList.Find(b1);
            myList.AddAfter(node1, b4);

            LinkedListNode<Book> node = myList.First;

            // Вариант 1 (проходим элементы с начала в конец) 
            while (node != null)
            {
                Console.WriteLine("Название=" + node.Value.BookName + " ,Цена=" + node.Value.Price);
                node = node.Next; 
            }

            // Вариант 2 (проходим элементы с конца в начало) 
            node = myList.Last;
            while (node != null)
            {
                Console.WriteLine("Название=" + node.Value.BookName + " ,Цена=" + node.Value.Price);
                node = node.Previous; 
            }
            Console.ReadLine();
        }
    }
}
