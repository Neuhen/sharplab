﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    class Point
    {
        private int[] array = new int [3];
        double mass;

        public void setX (int x)
        {
            this.array[0] = x;
        }

        public void setY(int y)
        {
            this.array[1] = y;
        }

        public void setZ(int z)
        {
            this.array[2] = z;
        }

        public void setMass(double mass)
        {
            if (mass < 0)
            {
                this.mass = 0;
            }
            else
            {
                this.mass = mass;
            }
        }

        public int getX()
        {
            return array[0];
        }

        public int getY()
        {
            return array[1];
        }

        public int getZ()
        {
            return array[2];
        }
        public double getMass()
        {
            return mass;
        }

        public Boolean isZero()
        {
            if(getX() == 0 && getY() == 0 && getZ() == 0)
            {
                return true;
            }
            else { return false; }
            
        }

        public double distanceBetweenTwoPoints(Point point2)
        {
            double distance = 0;

            distance = Math.Sqrt(Math.Pow(getX() - point2.getX(), 2) + Math.Pow(getY() - point2.getY(), 2) + Math.Pow(getZ() - point2.getZ(), 2));
            
            return distance;
        }
    }
}
