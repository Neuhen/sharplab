﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static void Main(string[] args)
    {
        int a, b;

        Console.Write("input a = ");
        a = Convert.ToInt32(Console.ReadLine());

        Console.Write("input b = ");
        b = Convert.ToInt32(Console.ReadLine());

        for (int i = a; i <= b; i++)
        {
            if (Calc.count_of_1(Calc.convert(i)) == 4)
            {
                Console.Write(i + "  ");
            }

        }   
       
        Console.ReadKey();
    }
}
