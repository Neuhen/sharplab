﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Message_WF_
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = Convert.ToString(Write(Convert.ToString(textBox1.Text)));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = Convert.ToString(Read(Convert.ToString(textBox1.Text)));
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        public static string Write(string str)
        {
            
            string path1 = @"C:\Users\smaga\Desktop\Ready to send.txt";
            int tmp = 2;
            StreamWriter txt = new StreamWriter(path1, false);
            char[] arr = str.ToCharArray();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (Convert.ToChar(arr[i] + tmp));
            }
            string str1 = new string(arr);
            txt.WriteLine(str1);
            txt.Close();
            return str1;
        }
        public static string Read(string str)
        {
            int tmp = 2;
            char[] arr = str.ToCharArray();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (Convert.ToChar(arr[i] - tmp));
            }
            string str1 = new string(arr);
            return str1;
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            trackBar1.Minimum = 1;
            trackBar1.Maximum = 10;
        }
    }
}
