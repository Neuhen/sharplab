﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Calc
{
    public static Stack<int> convert(int i)
    {
        var stack = new Stack<int>();

        while (i > 0)
        {
            stack.Push(i % 2);
            i /= 2;
        }
        return stack;
    }
        
    

    public static int count_of_1(Stack<int> stack)
    {
        int k = 0;

        while (stack.Count>0)
        {

                if (stack.Pop() == 1)
                {
                    
                    k++;
                }
        }
        return k;
    }

}
