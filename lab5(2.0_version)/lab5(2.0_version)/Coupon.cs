﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Coupon
{
    public delegate void coupon(Coupon cp);
    public event coupon give_coupon;
    public event coupon pometka_coupon;
    string zapis_name, pometka1;
    string dt1;
    public Coupon(string dt1, string zapis_name, string pometka1)
    {
        this.dt1 = dt1;
        this.zapis_name = zapis_name;
        this.pometka1 = pometka1;
    }
    public void appointment(List<Coupon> coupon_list)
    {

        Console.WriteLine("Введите имя спеца: ");
        string zapis_name = Console.ReadLine();
        Console.WriteLine("Введите дату приёма: ");
        dt1 = Console.ReadLine();
        coupon_list.Add(new Coupon(dt1, zapis_name, "не выполнено"));
        foreach (var i in coupon_list)
        {
            give_coupon(i);
        }
    }

    public void pometka(List<Coupon> coupon_list)
    {
        Console.WriteLine("Введите имя спеца: ");
        string zapis_name = Console.ReadLine();
        foreach (var d in coupon_list)
        {
            if (d.zapis_name == zapis_name)
            {
                d.pometka1 = "  выполнено";
                pometka_coupon(d);
            }

        }


    }
        public override string ToString()
        {
        return String.Format("{0, 10}{1, 5}{2, 5}", dt1, zapis_name, pometka1);
        }
}