﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
    class Trening
    {
        object[] treningArr;

        public void add(params Object[] cl)
        {
            treningArr = new object[cl.Length];

            for (int i = 0; i < cl.Length; i++)
            {
                treningArr[i] = cl[i];
            }            
        }

        public bool isPractical()
        {
            bool b = true;

            if (treningArr == null)
            {
                b = false;
                return b;
            }
                
            for (int i = 0; i < treningArr.Length; i++)
            {
                if (treningArr[i].GetType() == typeof(Lecture))
                {
                    b = false;
                    break;
                }
            }
            return b;
        }
    }
}
