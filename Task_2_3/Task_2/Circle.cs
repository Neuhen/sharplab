﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
    class Circle : Figure
    {
        private int radius;
        private Color color;

        public Circle(int radius, Color color)
        {
            this.radius = radius;
            this.color = color;
        }

        public int Radius
        {
            get { return radius; }
            set
            {
                try
                {
                    if (value < 0)
                    {
                        throw new Exception("Radius can't be < 0");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Exception : {e.Message}");
                    Console.ReadKey();
                    Environment.Exit(-1);
                }

            }
        }

        public double AreaOfCircle()
        {
            return Math.Pow(radius, 2) * Math.PI;
        }
    }
}
