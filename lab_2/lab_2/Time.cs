﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Time
{
    public static int addition, subtraction;
    public Time(int hour, int min, int sec)
    {
        this.hour = hour;
        this.min = min;
        this.sec = sec;
    }
    private int hour, min, sec;
    public int Get_Hour
    {
        get => hour;
        set => hour = value;
    }
    public int Get_Min
    {
        get => min;
        set => min = value;
    }
    public int Get_Sec
    {
        get => sec;
        set => sec = value;
    }

    public void subtraction_f(int hour, int min, int sec, int sec1)
    {
        subtraction = hour * 3600 + min * 60 + sec - sec1;
        hour = subtraction / 3600;
        subtraction = subtraction - hour * 3600;
        min = subtraction / 60;
        subtraction = subtraction - min * 60;
        sec = subtraction;
        Console.WriteLine($"subtraction {hour}:{min}:{sec}");

    }
    public void addition_f(int hour, int min, int sec, int sec1)
    {
        addition = hour * 3600 + min * 60 + sec + sec1;
        hour = addition / 3600;
        addition = addition - hour * 3600;
        min = addition / 60;
        addition = addition - min * 60;
        sec = addition;
        Console.WriteLine($"addition {hour}:{min}:{sec}");

    }
    public void difference(int hour1, int min1, int sec1, int hour2, int min2, int sec2)
    {
        int summ1 = hour1 * 3600 + min1 * 60 + sec1;
        int summ2 = hour2 * 3600 + min2 * 60 + sec2;
        if (summ1 > summ2)
        {
            ;
            Console.WriteLine($"time1 > time2 { Math.Abs(summ1 - summ2)}");
        }
        else
        {
            Console.WriteLine($"time1 < time2 { Math.Abs(summ1 - summ2)}");
        }
    }
}