﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
    class Matrix
    {
        int[] diagonal_elements, i, j;
        string[] substring;

        public Matrix(String str)
        {
            substring = str.Split(new char[] { ' ' });
            for(int i=0; i < substring.Length; i++)
            {
                diagonal_elements[i] = Convert.ToInt32(substring[i]);

            }
        }

        public int get_size_matrix()
        {
            return diagonal_elements.Length;
        }

        public int Track()
        {
            int sum = 0;

            for (int i = 0; i < diagonal_elements.Length; i++)
            {
                sum += diagonal_elements[i];
            }
            
            return sum;
        }

        public int get_element_by_index()
        {
            int i, j;
            Console.WriteLine("Введите i: ");
            i = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Введите j: ");
            j = Convert.ToInt32(Console.ReadLine());

            if (i != j)
            {
                return 0;
            }
            else {

                return diagonal_elements[i];
                 }
        }

    }
}
