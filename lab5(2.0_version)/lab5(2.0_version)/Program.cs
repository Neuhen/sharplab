﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static void Main(string[] args)
    {
        Coupon cp = new Coupon("","","");
        cp.give_coupon += Show_Message;
        cp.pometka_coupon += Show_Message;
        List<Coupon> coupon_list = new List<Coupon>();
        List <Doctor> list_of_doctors = new List<Doctor>();
        list_of_doctors.Add (new Doctor("Jack", "Melton", "General surgery"));
        list_of_doctors.Add(new Doctor("Gregory", "House", "Diagnostics"));
        list_of_doctors.Add(new Doctor("Adam", "Parker", "Pediatrics"));
        list_of_doctors.Add(new Doctor("William", "Gordon", "Pediatrics"));
        list_of_doctors.Add(new Doctor("Myron", "McDowell", "General surgery"));
        list_of_doctors.Add(new Doctor("Robert", "Atkins", "Diagnostics"));

        int choise;
        do
        {
            Console.WriteLine(" 1 - список специалистов \n 2 - выдать талон \n 3 - занести пометку в карту \n 4 - выход");
            choise = Convert.ToInt32(Console.ReadLine());

            switch (choise)
            {
                case 1:
                    Registry.show(list_of_doctors);
                    break;
                case 2:
                    cp.appointment(coupon_list);
                    break;
                case 3:
                    cp.pometka(coupon_list);
                    break;
            }
        } while (choise != 4);
    }
    public static void Show_Message(Coupon d)
    {
        Console.WriteLine(d.ToString());
    }
}