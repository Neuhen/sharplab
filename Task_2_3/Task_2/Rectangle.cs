﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
    class Rectangle : Figure
    {
        private int width;
        private int height;
        private Color color;

        public Rectangle(int width, int height, Color color)
        {
            this.height = height;
            this.width = width;
            this.color = color;
        }

        public int Width
        {
            get { return width; }
            set
            {
                width = checkValue(value, "Width can't be < 0 ");
            }
        }

        public int Height
        {
            get { return height; }
            set
            {
                height = checkValue(value, "Height can't be < 0 ");
            }
        }

        public new Color Color
        {
            get { return Color; }
            set { Color = value; }
        }

        public int checkValue(int value, string str)
        {
            try
            {
                if (value < 0)
                {
                    throw new Exception(str);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception : {e.Message}");
                Console.ReadKey();
                Environment.Exit(-1);
            }
            return value;
        }

        public int AreaOfRectangle(int width, int height)
        {
            return width * height;
        }
    }
}
