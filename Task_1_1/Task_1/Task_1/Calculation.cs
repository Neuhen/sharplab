﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Calculation
{
    public static double calc (double T, double v)
    {
        double w = 35.74 + 0.6215 * ConvertVar.convert_to_F(T) + Math.Abs((0.4275 * T - 35.75)) * Math.Pow(ConvertVar.convert_to_mps(v), 0.16);

        return w;
    }
}

