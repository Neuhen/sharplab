﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
    class PracticalLesson
    {
        private string condition;
        private string decision;

        public string getCondition() => condition;
        public string getDecision() => decision;
    }
}
