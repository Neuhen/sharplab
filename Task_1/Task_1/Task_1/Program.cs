﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    class Program
    {
        static void Main(string[] args)
        {
            double T, v;
            
            Console.Write("Input T in celsia = ");
            T = Convert.ToDouble(Console.ReadLine());
        
            Console.Write("Input v in m/s = ");
            v = Convert.ToDouble(Console.ReadLine());

            Console.Write("w = " + Calculation.calc(T, v));

            Exceptional.input_check(T, v); 

            Console.ReadKey();
        }
    }
