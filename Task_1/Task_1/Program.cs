﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Point point1 = new Point();
            point1.setX(1);
            point1.setY(2);
            point1.setZ(3);
            point1.setMass(-5);

            Point point2 = new Point();
            point2.setX(3);
            point2.setY(2);
            point2.setZ(1);
            point2.setMass(-5);
            
            Console.WriteLine(point1.distanceBetweenTwoPoints(point2));
            Console.WriteLine(point1.isZero());

            Console.ReadKey();
        }
    }
}
