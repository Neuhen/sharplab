﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

abstract class Animal
{
    public virtual void Serach_eat()
    {

    }
    public abstract String Voice { set; get; }
    public abstract String Eat { set; get; }
}

class Lion : Animal
{
    public override void Serach_eat()
    {
        Console.WriteLine("serch lion");
    }
    public String eat;
    public override String Eat
    {
        set => eat = "meat";
        get => eat;
    }
    public override String Voice
    {
        set => Voice = "rrr";
        get => Voice;
    }
}
class Giraffe:Animal 
{
    String eat;
    public override void Serach_eat()
    {
        Console.WriteLine("serch giraffe");
    }
    public override String Eat
    {
        set => eat = "leafs";
        get => eat;
    }
    public override String Voice
    {
        set => Voice = "giraffe";
        get => Voice;
    }
}
class Chimpanzee : Animal
{
    String eat;
    public override void Serach_eat()
    {
        Console.WriteLine("chimpanzee");
    }
    public override String Eat
    {
        set => eat = "bananas";
        get => eat;
    }
    public override String Voice
    {
        set => Voice = "uu aa";
        get => Voice;
    }
}