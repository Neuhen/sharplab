﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Trening tr1 = new Trening();
            tr1.add(new Lecture(), new PracticalLesson());
            Console.WriteLine(tr1.isPractical());

            Trening tr2 = new Trening();
            tr2.add(new PracticalLesson());
            Console.WriteLine(tr2.isPractical());

            Trening tr3 = new Trening();
            Console.WriteLine(tr3.isPractical());

            Console.ReadKey();
        }
    }
}
